# library-webApp

Simple CRUD operations on books and authors 


## Creating a virtual environment 
    python3 -m venv env

or
### Python2

(when installing  virtualenv ) 

    py -m pip install --user virtualenv
    py -m virtualenv env 

## Using the library-webApp

Installing the necessary frameworks using pip 

    -   pip install django 
    -   pip install djangorestframework
    -   pip install PyMySQL
    -   pip install django-cors-headers

### Regarding the database, 
        In the settings.py file  (located in  libraryBEnd\libraryBEnd ) change the name of the database accordingly.

## Pages 
### Books page
In this page we can see all the books listed. 
This page is aimed towards the admin, but since I have not included auth, it is visible by everyone. 
People can add, edit or delete books.

### Readers page 
In this page we can see all the readers listed  , including the book they are currently reading .

### Pick a book page 
In this page , readers can pick from the available books to read . By available I mean, the current stock of the book is greater than zero. 
