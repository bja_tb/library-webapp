import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookComponent } from './book/book.component';
import { ReaderComponent } from './reader/reader.component';
import { PickComponent } from './pick/pick.component' ;

const routes: Routes = [
  { path: '', component: BookComponent},
  { path: 'books', component: BookComponent},
  { path: 'readers', component: ReaderComponent},
  { path: 'pick', component: PickComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

// method is called forRoot() because u configure the router at the application's root level

export class AppRoutingModule { }
