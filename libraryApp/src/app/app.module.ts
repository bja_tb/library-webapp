import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularMaterialModule } from '../angular-material/angular-material.module';

import { HttpClientModule } from '@angular/common/http';
import { BookComponent } from './book/book.component';
import { ReaderComponent } from './reader/reader.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { PickComponent } from './pick/pick.component';
// import { Book } from './book';

@NgModule({
  declarations: [
    AppComponent,
    BookComponent,
    ReaderComponent,
    PickComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    NoopAnimationsModule,
    AngularMaterialModule,
    FormsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
