import { Component, OnInit, Input } from '@angular/core';

import { BookService } from '../services/book.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css'],
})
export class BookComponent implements OnInit {
  del: any;

  bookList: any = [];

  headElements = ['Title', 'Description', 'Published', 'Stock', 'Action'];

  // ID do merret kur te shtypet vete butoni
  // do perdoret kur ta shkruajme ne db , using ID

  showDiv: boolean;
  showDivAdd: boolean;
  showDivEdit: boolean;
  @Input()
  id!: number;
  @Input() value: Date | String = '';
  // @Output() dateInput: EventEmitter<MatDatepickerInputEvent<String|Date>>= new EventEmitter();
  @Input() newTitle: String = '';
  @Input() newDescription: String = '';
  @Input() newStock!: String;
  // stock!:number;
  getIn(event: Event) {
    return (event.target as HTMLInputElement).value;
  }

  constructor(private bookService: BookService) {
    this.showDiv = true;
    this.showDivAdd = false;
    this.showDivEdit = false;
  }

  ngOnInit(): void {
    // called when first loaded
    this.getBooks();
  }

  submitted = false;

  onSubmit() {
    this.submitted = true;
  }

  getBooks(): void {
    this.bookService.getBooks().subscribe(
      (response) => {
        this.bookList = response.results;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  deleteBook(book: any) {
    // console.log(book.id);

    if (confirm('Are you sure you want to delete book? ')) {
      this.bookService.deleteBookById(book.id).subscribe(
        (response) => {
          // console.log(response);

          if (response.ok) {
            this.getBooks();
            // I need to add maybe a confirmation message xD, yay
          }
        },
        (error: any) => {
          console.log(error);
        }
      );
    }

    // console.log(event)
  }
  pressEdit(book: any) {
    this.showDiv = false;
    this.showDivEdit = true;
    this.newTitle = book.title;
    this.value = book.published;
    this.newDescription = book.description;
    this.id = book.id;
    this.newStock=book.stock;
  }

  clicked() {
    this.showDivEdit = false;
    this.showDiv = true;
    this.bookService.updateBookById(this.id, {
        title: this.newTitle,
        description: this.newDescription,
        published: this.value,
        stock:this.newStock
      })
      .subscribe(
        (response) => {
          // console.log(response);
         this.getBooks();
        },
        (error: any) => {
          console.log(error);
        }
      );
  }

  add() {
    this.showDivAdd = true;
    this.showDiv = false;
  }

  added(bookTitle: string, bookDesc: string, bookPub: string, stock: string) {
    this.showDivAdd = false;
    this.showDiv = true;

    let book = {
      title: bookTitle,
      description: bookDesc,
      published: bookPub,
      stock: stock,
    };

    this.bookService.createBook(book).subscribe(
      (response) => {
        // console.log(response);

        this.getBooks();
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  goBack() {
    this.showDivAdd = false;
    this.showDiv = true;
  }
}
