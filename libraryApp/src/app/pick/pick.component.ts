import { Component, OnInit, Input } from '@angular/core';
import { BookService } from '../services/book.service';
import { ReaderService } from '../services/reader.service';
@Component({
  selector: 'app-pick',
  templateUrl: './pick.component.html',
  styleUrls: ['./pick.component.css'],
})
export class PickComponent implements OnInit {
  bookList: any = [];

  previousBook!: any;

  currentBookID!: number;
  currentBookTitle!: string;
  currentBookStock!: number;
  currentBookDescription!: string;
  currentBookPublished!: Date;

  headElements = ['Title', 'Description', 'Published', 'Action'];
  readerInfo!: any;
  showDiv: boolean;

  @Input()
  id!: number;

  constructor(
    private bookService: BookService,
    private readerService: ReaderService
  ) {
    this.showDiv = true;
  }

  ngOnInit(): void {
    this.getBooks();
  }

  getBooks(): void {
    this.bookService.getBooks().subscribe(
      (response) => {
        this.bookList = response.results;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  readBook(book: any) {
    this.showDiv = false;
    // there is no problem with this setting , since we are picking from the available ones
    this.currentBookID = book.id;
    this.currentBookTitle=book.title;
    this.currentBookDescription=book.description;
    this.currentBookPublished=book.published;
    this.currentBookStock=book.stock;

    console.log()

    }

  added() {
    this.bookService.updateBookById(this.currentBookID, {
      title: this.currentBookTitle,
      description: this.currentBookDescription,
      published: this.currentBookPublished,
      stock:this.currentBookStock-1
    })
    .subscribe(
      (response) => {
        // console.log(response);
       this.getBooks();
      },
      (error: any) => {
        console.log(error);
      }
    );

  // we update the reader successfully
   
      
    // // getting the reader who just picked a book
    this.readerService.getReaderById(this.id).subscribe(
      (response) => {
        this.readerInfo = response;
       console.log(this.readerInfo.name);
       
       this.bookService.getBookById(this.readerInfo.currentBook).subscribe(
        (response)=>{this.previousBook=response;
          if(this.readerInfo.currentlyReading) {
            console.log("hellodkjflsdkf")
            this.bookService.updateBookById(this.readerInfo.currentBook, {
              title: this.previousBook.title,
              description: this.previousBook.description,
              published: this.previousBook.published,
              stock:this.previousBook.stock + 1
            })
            .subscribe(
              (response) => {
                // console.log(response);
               this.getBooks();
              },
              (error: any) => {
                console.log(error);
              }
            );
            }
        
        },
        (error: any)=>{console.log(error);}
      )
       
      },
      (error: any) => {
        console.log(error);
      }
    );
        
    this.readerService
    .updateReaderById(this.id, {
      currentlyReading: true,
      currentBook: this.currentBookID,
    })
    .subscribe(
     ()=>{},
      (error: any) => {
        console.log(error);
      }
    );
      
   
      this.showDiv=true;
  }
}
