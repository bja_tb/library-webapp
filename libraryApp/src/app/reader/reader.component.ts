import { Component, Input, OnInit } from '@angular/core';
import { ReaderService } from '../services/reader.service';
import { BookService } from '../services/book.service';
// import { stringify } from 'querystring';


@Component({
  selector: 'app-reader',
  templateUrl: './reader.component.html',
  styleUrls: ['./reader.component.css']
})
export class ReaderComponent implements OnInit {

  bookTitles :any;
  readerList:any;

  

  headElements = ['ID', 'Name', 'Surname', 'Currently Reading', 'Is reading'];

  constructor(private readerService : ReaderService, private bookService:BookService) { }

  ngOnInit(): void {
    
    this.readerService.getReaders().subscribe(

    (response)=>{
                this.readerList=response.results},

    (error) => { console.log(error)}
    
    )

  
   
    }
  
    
  }

 

