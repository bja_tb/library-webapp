import { ThrowStmt } from '@angular/compiler';
import { Injectable, Input } from '@angular/core';
import { Observable } from 'rxjs';
// import { HttpHeaders } from '@angular/common/http';
import { HttpService } from '../http.service/http.service';

@Injectable({
  providedIn: 'root',
})
export class BookService {
  constructor(private http: HttpService) {}

  public getBooks(): Observable<any> {
    return this.http.GET('books/', '');
  }

  public getBookById(id:number): Observable<any> {
    return this.http.GET('books/'+id+'/', '');
  }

  public deleteBookById(id: number): Observable<any> {
    return this.http.DELETE('books/delete/' + id + '/');
  }

  public updateBookById(id: number, model: any): Observable<any> {
    return this.http.PUT('books/update/' + id + '/', model, '');
  }

  public createBook(model:any): Observable<any> {
    return this.http.POST('books/add/',model,'');
  }
}
