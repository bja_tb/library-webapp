import { Injectable } from '@angular/core';
import { HttpService } from '../http.service/http.service'
import { Observable, } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReaderService {


  constructor(private http: HttpService) {}

  public getReaders(): Observable<any> {
    return this.http.GET('readers/', '');
  }

  public updateReaderById(id: number,model:any): Observable<any> {
    return this.http.PUT('readers/update/' + id + '/', model, '');
  }

  public getReaderById(id:number): Observable<any> {
    return this.http.GET('readers/'+id+'/', '');
  }
}
