# Generated by Django 4.0.4 on 2022-07-14 10:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('restL', '0002_alter_reader_currentbook'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reader',
            name='currentBook',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='restL.book'),
        ),
    ]
