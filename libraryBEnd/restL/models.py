
from django.db import models

# Create your models here.

class Book(models.Model):
    title = models.CharField(max_length=70, blank=False, default='', unique=True)
    description = models.CharField(max_length=200,blank=False, default='')
    published = models.DateField(default=False)
    
    stock = models.PositiveIntegerField()
    def __str__(self) :
        return self.title
  
class Reader(models.Model):
    name = models.CharField(max_length=70, blank=False, default='')
    surname = models.CharField(max_length=70, blank=False, default='')
    currentlyReading = models.BooleanField(default=False)
    currentBook = models.ForeignKey(Book,on_delete=models.CASCADE, null=True)   
    currentTitle = models.CharField(max_length=70, blank=False, default='')
    def __str__(self) :
        return self.name + self.surname
    
