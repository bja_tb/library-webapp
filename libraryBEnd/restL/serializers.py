from rest_framework import serializers
from restL.models import Reader
from restL.models import Book

class RestLSerializer(serializers.ModelSerializer):

    class Meta:
        model = Book
        fields = ["id", "title", "description", "published", "stock"]


class ReaderSerializer(serializers.ModelSerializer):
    # currentBook=RestLSerializer()

    currentTitle = serializers.PrimaryKeyRelatedField(source="currentBook.title",  many=False,read_only=True)

    class Meta:
        model = Reader
        fields = ["id","name", "surname", "currentlyReading", "currentBook", "currentTitle"]

    # def create(self, validated_data):
    #     currentBook_data = validated_data.pop('currentBook')
    #     reader = Reader.objects.create(**validated_data)
    #     for book_data in currentBook_data:
    #         Book.objects.create(reader=reader, **book_data)
    #     return reader