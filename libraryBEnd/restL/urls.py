from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from restL import views




# API endpoints
urlpatterns = format_suffix_patterns([
    path('', views.api_root),

    path('books/',
        views.book_list.as_view(),
        name='book-list'),
    path('books/<int:pk>/',
        views.book_detail.as_view(),
        name='book-detail'),
    path('readers/',
        views.reader_list.as_view(),
        name='reader-list'),
    path('readers/<int:pk>/',
        views.reader_detail.as_view(),
        name='reader-detail'),
    path('books/update/<int:pk>/',
        views.update_book, 
        name='update-book'),
    path('readers/update/<int:pk>/',
        views.update_reader, 
        name='update-reader'),
    path('books/delete/<int:pk>/', 
         views.delete_book, 
         name='delete-book'),
    path('books/add/', views.create_book, name='create-book'),
   
  
])
    # path('activity/', views.activity_list.as_view(), name='activity-list'),
    
       