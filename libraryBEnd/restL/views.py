from rest_framework.reverse import reverse
from rest_framework.response import Response
from restL.models import Book, Reader
from restL.serializers import RestLSerializer, ReaderSerializer
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework import status
from django.shortcuts import get_object_or_404  # for the update view


class book_list(generics.ListCreateAPIView):
    queryset = Book.objects.all()
    serializer_class = RestLSerializer


class book_detail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Book.objects.all()
    serializer_class = RestLSerializer


class reader_list(generics.ListCreateAPIView):
    queryset = Reader.objects.all()
    serializer_class = ReaderSerializer


class reader_detail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Reader.objects.all()
    serializer_class = ReaderSerializer


@api_view(["GET"])
def api_root(request, format=None):
    return Response(
        {
            "readers": reverse("reader-list", request=request, format=format),
            "books": reverse("book-list", request=request, format=format),
          
        }
    )


@api_view(["GET", "POST"])
def create_book(request):
    # """
    # List all code snippets, or create a new snippet.
    # """
    # if request.method == "GET":
    #     snippets = Book.objects.all()
    #     serializer = RestLSerializer(snippets, many=True)
    #     return Response(serializer.data)

    if request.method == "POST":
        serializer = RestLSerializer(data=request.data, context={"request": request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(["GET","PUT"])
def update_book(request, pk):
    item = Book.objects.get(pk=pk)
    data = RestLSerializer(
        instance=item, data=request.data, context={"request": request}
    )

    if data.is_valid():
        data.save()
        return Response(data.data)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)

@api_view(["GET","PUT"])
def update_reader(request, pk):
    item = Reader.objects.get(pk=pk)
    data = ReaderSerializer(
        instance=item, data=request.data, context={"request": request}
    )

    if data.is_valid():
        data.save()
        return Response(data.data)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(["DELETE"])
def delete_book(response, pk):
    item = get_object_or_404(Book, pk=pk)
    item.delete()
    return Response(status=status.HTTP_202_ACCEPTED)
